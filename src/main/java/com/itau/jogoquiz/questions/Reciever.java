package com.itau.jogoquiz.questions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.itau.jogoquiz.questions.models.Question;
import com.itau.jogoquiz.questions.repositories.QuestionRepository;

@Component
public class Reciever {
	//public static int quantidade;
	
	@Autowired
	static QuestionRepository questionRepository;
	
	@JmsListener(destination = "c1.queue.questions.qtd")
	public static void recieveMessage(String msg) {
		//Reciever.quantidade = quantidade;
		//System.out.println("Solicitacao recebida < " + quantidade + ">");
		int quantidade = Integer.parseInt(msg);
		Iterable<Question> questions = questionRepository.findAll();
		List<Question> questoes = Lists.newArrayList(questions);
		List<Long> questoesRetorno = new ArrayList<Long>();
		for (int i = 0; i < quantidade; i++) {
			int indice = (int) Math.floor(Math.random() * quantidade);
			questoesRetorno.add(questoes.get(indice).getId());
			questoes.remove(indice);
		}
		try {
			Sender.sendMessage(questoesRetorno);
			//return ResponseEntity.ok().body(questoesRetorno);
		
		} catch (Exception e) {
			System.out.println("Erro: "+ e.getMessage());
			//return ResponseEntity.badRequest().build();
		}
	}
}
