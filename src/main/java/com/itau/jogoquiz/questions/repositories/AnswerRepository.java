package com.itau.jogoquiz.questions.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.jogoquiz.questions.models.Answer;

public interface AnswerRepository extends CrudRepository<Answer, Long> {
	

}
