package com.itau.jogoquiz.questions;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class Sender {
	
	@Autowired
	private ConnectionFactory connectionFactory;
	private static JmsTemplate jmsTemplate;
			
	@PostConstruct
	public void init() {
		Sender.jmsTemplate = new JmsTemplate(connectionFactory);
	}
	
	public static void sendMessage(List<Long> listaEnvio) {
		System.out.println("Chegou no metodo de fila");		
		jmsTemplate.convertAndSend("c1.queue.questions.id",listaEnvio);
		System.out.println("Enviou para a fila");
	}

}
