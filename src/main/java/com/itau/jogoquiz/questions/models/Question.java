package com.itau.jogoquiz.questions.models;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Question {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotNull
	private String pergunta;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<Answer> respostas;
	
	

	public long getId() {
		return id;
	}
	public List<Answer> getRespostas() {
		return respostas;
	}
	public void setRespostas(List<Answer> respostas) {
		
		this.respostas = respostas;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPergunta() {
		return pergunta;
	}
	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}



}
