package com.itau.jogoquiz.questions.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.jogoquiz.questions.models.Answer;
import com.itau.jogoquiz.questions.repositories.AnswerRepository;

@RestController
public class AnswerController {
	
	@Autowired
	AnswerRepository answerRepository;

	@RequestMapping(method=RequestMethod.POST, path="/answer")
	public Answer criarResposta(@Valid @RequestBody Answer resposta) {
		return answerRepository.save(resposta);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/answerList")
	public Iterable<Answer> buscarRespostas() {
		return answerRepository.findAll();
	}


}
