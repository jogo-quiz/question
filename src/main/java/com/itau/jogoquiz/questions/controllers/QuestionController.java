package com.itau.jogoquiz.questions.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.apache.log4j.spi.LoggerFactory;
import org.apache.logging.log4j.message.MapMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;
import com.itau.jogoquiz.questions.Reciever;
import com.itau.jogoquiz.questions.Sender;
import com.itau.jogoquiz.questions.models.Question;
import com.itau.jogoquiz.questions.repositories.QuestionRepository;
import com.itau.jogoquiz.questions.services.Consumer;
import com.itau.jogoquiz.questions.services.Producer;

import ch.qos.logback.classic.Logger;

@RestController
public class QuestionController {

//	@Autowired
//	Sender question; 

	@Autowired
	QuestionRepository questionRepository;

	@RequestMapping(method = RequestMethod.POST, path = "/questioadd")
	public Question criarpergunta(@Valid @RequestBody Question question) {
		return questionRepository.save(question);
	}

	private Class<? extends Class> log = LoggerFactory.class.getClass();
	
	// Funcao de retornar pergunta por ID
	@RequestMapping(method = RequestMethod.GET, path = "/question/{id}")
	public ResponseEntity<?> listarQuestao(@PathVariable int id) {
		Optional<Question> questions = questionRepository.findById((long) id);
		String body = "C1.Jogo requisitou a pergunta: "+ id;
		Thread a = Producer.createProducerThread(id, body);
		System.out.println(a);
		a.start();	
		return ResponseEntity.ok().body(questions);
	}

	// Funcao para retornar uma lista de perguntas de acordo com a quantidade
	// desejada

	// int quantidade = Reciever.quantidade;

	@RequestMapping(method = RequestMethod.GET, path = "/questions/{quantidade}")

	public ResponseEntity<?> listarQuestionario(@PathVariable int quantidade) {

		Iterable<Question> questions = questionRepository.findAll();

		List<Question> questoes = Lists.newArrayList(questions);
		List<Long> questoesRetorno = new ArrayList();

		for (int i = 1; i <= quantidade; i++) {
			// int indice = (int) Math.floor(Math.random() * quantidade);
			int indice = (int) (Math.random() * quantidade);
			
			if (indice < questoes.size()) {
				questoesRetorno.add(questoes.get(indice).getId());
				questoes.remove(indice);
				// Envio de log para o Kafka
				String body = "C1.Inculida a pergunta : "+ indice + "no jogo";
				Thread a = Producer.createProducerThread(indice, body);
				a.start();
				// Recepcao de log do Kafka
				
				
			}
		}
		try {
			// Sender.sendMessage(questoesRetorno);
			Consumer.inicio();
			return ResponseEntity.ok().body(questoesRetorno);
			

		} catch (Exception e) {
			System.out.println("Erro: " + e.getMessage());
			String body = "Deu erro no processamento da lista de perguntas: " + e.getMessage();
			Thread a = Producer.createProducerThread(quantidade, body);
			a.start();
			return ResponseEntity.badRequest().build();

		}
	}

}
