package com.itau.jogoquiz.questions.services;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;


public class Producer {
	private static final int P_THREADS = 5;
	private static final String TOPIC = "questions";
	private static final String SERVERS = "10.162.107.229:9090;10.162.107.229:9091;10.162.107.229:9092";
	
	public static KafkaProducer<String, String> createProducer(int index) {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, SERVERS);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "C1" + index);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

		KafkaProducer<String, String> producer = new KafkaProducer<>(props);
		return producer;
	}
	
	public static Thread createProducerThread(final int index , final String log) {
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				System.out.printf("Thread [%s] starting.\n", Thread.currentThread().getName());
				Random random = new Random();
				KafkaProducer<String, String> producer = createProducer(index);
//				for (long i = 0; i < 10; i++) {
					
					final String topico = TOPIC;
					final String key = "c1.log." + random.nextInt(10000);
					final String body = log + key;
					System.out.println(body);
					ProducerRecord<String, String> record = new ProducerRecord<>(topico, key, body);
					System.out.println(record);
					try {
						producer.send(record).get();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
//					producer.send(record, (m, e) -> {
//						System.out.println(" [x] Sent " + index + "/" + mid);
//					});
//				}
				producer.flush();
				producer.close();
			}
		});
		t.setName("ProducerThread-" + index);
		return t;
	}


	
			
}
