package com.itau.jogoquiz.questions.services;

import java.time.Duration;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;




import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;





public class Consumer {
	private static final int C_THREADS = 5;
	private static final String GROUPS[] = new String[] {"C1"};
	
	private static final String TOPIC = "questions";
	private static final String SERVERS = "10.162.107.229:9090;10.162.107.229:9091;10.162.107.229:9092";

	public static KafkaConsumer<String, String> createConsumer(String group, int index) {
		Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, SERVERS);
		props.put(ConsumerConfig.CLIENT_ID_CONFIG, "C1" + index);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, group);
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

		KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
		return consumer;
	}

	public static Thread createConsumerThread(final String group, final int index) {
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.printf("Thread [%s] starting.\n", Thread.currentThread().getName());
				KafkaConsumer<String, String> consumer = createConsumer(group, index);
				consumer.subscribe(Collections.singletonList(TOPIC));
				
				try {
					while (true) {
						
						ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(500));

						for (ConsumerRecord<String, String> record : records) {
							System.out.printf(
									" [%s] Consumed: topic=[%s] partition=[%d] offset=[%d] key=[%s] value=[%s] \n",
									Thread.currentThread().getName(),
									record.topic(),
									record.partition(),
									record.offset(),
									record.key(),
									record.value());
						}
					}
				} finally {
					consumer.close();
				}
			}
		});
		t.setName("ConsumerThread-" + group+"-" + index);
		return t;
	}
	
	public static void inicio() {
		List<Thread> threads = new LinkedList<>();
		
		System.out.println("Starting threads...");
		
		// Criar threads de consumidores
		for(String group : GROUPS) {
			for(int j = 0; j < C_THREADS; j++) {
				Thread t = createConsumerThread(group, j);
				threads.add(t);
			}
		}
		
		
		// Inicia todas as threads
		for(Thread t : threads) {
			t.start();
		}

		try {
			System.out.println("Waiting threads...");

			for(Thread t : threads) {
				t.join();
			}

			System.out.println("Finished");

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	
	




}
